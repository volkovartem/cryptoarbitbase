import time
import json
import traceback

from rest import restKucoin as kucoin

from pymemcache.client.base import Client

# settings
# set updating interval in seconds
interval_balance = 1
interval_limits = 700

name = 'Kucoin'
timeout_balance = time.time() - interval_balance
timeout_limits = time.time() - interval_limits


def push_balance(client):
    global timeout_balance
    if time.time() - timeout_balance > interval_balance:
        balances = kucoin.get_balance()
        if balances is not None:
            client.set(name + 'Balance', json.dumps(balances))
            print('balances', len(balances), balances)
        timeout_balance = time.time()


def push_limits(client):
    global timeout_limits
    if time.time() - timeout_limits > interval_limits:
        limits = kucoin.get_trade_limits()
        if limits is not None:
            client.set(name + 'Limits', json.dumps(limits))
            print('limits', len(limits), limits)
        timeout_limits = time.time()


# updating loop
while True:
    try:
        m_client = Client(('127.0.0.1', 11211))
        push_balance(m_client)
        push_limits(m_client)
        time.sleep(0.01)
    except KeyboardInterrupt:
        break
    except:
        print(traceback.format_exc())
        time.sleep(5)
