from rest.wrapper import place_limit_order


def execute(exchange, symbol, side, amount, price, response):
    order = place_limit_order(exchange, symbol, side, amount, price)
    response.update({'order': order})
