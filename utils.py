import json
import os

from pymemcache.client.base import Client


def get_preferences(exchange):
    cur_path = os.path.dirname(__file__)
    file_path = cur_path + '/preferences.txt'
    ff = open(file_path, "r")
    preferences = json.loads(ff.read())
    ff.close()
    return preferences[exchange]


def get_price(exchange, symbol):
    m_client = Client(('127.0.0.1', 11211))
    data = m_client.get(symbol + '_' + exchange)
    if data is not None:
        return json.loads(data)

# ------------ usage ------------

# print(get_preferences('Bibox'))
# print(get_price('Bibox', 'BTC_USDT'))
