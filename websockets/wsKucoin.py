import json
import time
import traceback

import requests
import websocket
from pymemcache.client.base import Client

name = 'Kucoin'
client = Client(('127.0.0.1', 11211))


def on_message(ws, message):
    try:
        ticker = json.loads(message)
        if ticker['type'] == 'message':
            symbol = ticker['topic'].replace('/market/ticker:', '').replace('-', '_')

            data = {'timestamp': int(float(ticker['data']['time']) / 1000),
                    'ask': ticker['data']['bestAsk'],
                    'bid': ticker['data']['bestBid'],
                    'ask_amount': ticker['data']['bestAskSize'],
                    'bid_amount': ticker['data']['bestBidSize']}

            client.set(symbol + '_' + name, json.dumps(data))
            print(symbol, data)
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    print('### opened ###')

    pairs = ''
    for pair in pairList:
        pairs += pair.replace('_', '-') + ','

    try:
        d = {"id": 1545910660739,
             "type": "subscribe",
             "topic": "/market/ticker:" + pairs.strip(','),
             "privateChannel": False,
             "response": False}
        ws.send(json.dumps(d))
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


while True:
    print('start websocket {}'.format(name))
    time.sleep(.1)

    pairList = ['BTC_USDT', 'ETH_USDT']

    print(len(pairList), pairList)
    client.set('pairList_' + name, json.dumps(pairList))

    token = requests.post('https://api.kucoin.com/api/v1/bullet-public').json()['data']['token']

    try:
        ws = websocket.WebSocketApp(
            "wss://push1-v2.kucoin.com/endpoint?token=" + token + "&[connectId=7777]",
            on_open=on_open,
            on_message=on_message,
            on_error=on_error,
            on_close=on_close)

        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
