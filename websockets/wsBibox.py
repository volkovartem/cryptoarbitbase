import base64
import gzip
import json
import time
import traceback

import websocket
from pymemcache.client.base import Client

name = 'Bibox'
client = Client(('127.0.0.1', 11211))


def on_message(ws, message):
    try:
        if type(json.loads(message)) is list and 'channel' in json.loads(message)[0]:
            channel = json.loads(message)[0]['channel']
            symbol = channel.replace('bibox_sub_spot_', '').replace('_ticker', '')
            ticker = json.loads(gzip.decompress(base64.b64decode(json.loads(message)[0]['data'])))

            data = {'timestamp': int(float(ticker['timestamp']) / 1000),
                    'ask': ticker['sell'],
                    'bid': ticker['buy'],
                    'ask_amount': ticker['sell_amount'],
                    'bid_amount': ticker['buy_amount']}

            client.set(symbol + '_' + name, json.dumps(data))
            print(channel, symbol, data)
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    print('### opened ###')

    try:
        for pair in pairList:
            ws.send(json.dumps({
                "event": "addChannel",
                "channel": "bibox_sub_spot_{}_ticker".format(pair)
            }))
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


while True:
    print('start websocket {}'.format(name))
    time.sleep(.1)

    pairList = ['BTC_USDT', 'ETH_USDT']

    print(len(pairList), pairList)
    client.set('pairList_' + name, json.dumps(pairList))

    try:
        ws = websocket.WebSocketApp(
            "wss://push.bibox.com/",
            on_open=on_open,
            on_message=on_message,
            on_error=on_error,
            on_close=on_close)

        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
