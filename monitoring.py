import json
import threading
import time

from pymemcache.client.base import Client

from executor import execute
from utils import get_preferences
from utils import get_price

# settings
# age of data (in seconds)
latency_threshold = 20


def check_timestamp(exchange, opposite_exchange, exchange_price, opposite_exchange_price):
    if exchange_price['timestamp'] + latency_threshold < time.time():
        print('Latency more than 10 sec: check websocket {}'.format(exchange))
        return False
    if opposite_exchange_price['timestamp'] + latency_threshold < time.time():
        print('Latency more than 10 sec: check websocket {}'.format(opposite_exchange))
        return False
    return True


def find_opportunity(exchange, opposite_exchange):
    # print(exchange, opposite_exchange)
    m_client = Client(('127.0.0.1', 11211))

    # prepare a lists of pairs for both exchanges
    pair_list_for_exchange = m_client.get('pairList_{}'.format(exchange))
    pair_list_for_opposite_exchange = m_client.get('pairList_{}'.format(opposite_exchange))
    if pair_list_for_exchange is not None:
        pair_list_for_exchange = json.loads(pair_list_for_exchange)
    else:
        print('Launched ws{} is required'.format(exchange))
        return
    if pair_list_for_opposite_exchange is not None:
        pair_list_for_opposite_exchange = json.loads(pair_list_for_opposite_exchange)
    else:
        print('Launched ws{} is required'.format(opposite_exchange))
        return

    # look for positive difference between prices
    for pair in pair_list_for_exchange:
        if pair in pair_list_for_opposite_exchange:
            exchange_price = get_price(exchange, pair)
            opposite_exchange_price = get_price(opposite_exchange, pair)
            if check_timestamp(exchange, opposite_exchange, exchange_price, opposite_exchange_price):
                # print(exchange_price, opposite_exchange_price)
                difference_1 = round(1 / float(exchange_price['ask']) * float(opposite_exchange_price['bid']), 5)
                difference_2 = round(1 / float(opposite_exchange_price['ask']) * float(exchange_price['bid']), 5)
                if difference_1 > 1:
                    # if ask of exchange1 > bid of exchange2
                    exchange_ask_amount = float(exchange_price['ask_amount'])
                    opposite_exchange_bid_amount = float(exchange_price['bid_amount'])
                    amount = min(exchange_ask_amount, opposite_exchange_bid_amount)
                    # print('OPPORTUNITY', time.time(), exchange, pair, opposite_exchange, difference_1, amount)
                    execute_trade(exchange, opposite_exchange, pair, difference_1, amount,
                                  exchange_price['bid'], opposite_exchange_price['ask'])
                if difference_2 > 1:
                    # if ask of exchange2 > bid of exchange1
                    exchange_bid_amount = float(exchange_price['bid_amount'])
                    opposite_exchange_ask_amount = float(exchange_price['ask_amount'])
                    amount = min(opposite_exchange_ask_amount, exchange_bid_amount)
                    # print('OPPORTUNITY', time.time(), opposite_exchange, pair, exchange, difference_2, amount)
                    execute_trade(exchange, opposite_exchange, pair, difference_2, amount,
                                  opposite_exchange_price['ask'], exchange_price['bid'])


def execute_trade(exchange, opposite_exchange, pair, difference, amount, exchange_price, opposite_exchange_price):
    # print(exchange, opposite_exchange, pair, difference, amount)

    exchange_fee = float(get_preferences(exchange)['trading_fee'])
    opposite_exchange_fee = float(get_preferences(opposite_exchange)['trading_fee'])
    expected_profit = round(difference * 100 - 100 - exchange_fee - opposite_exchange_fee, 2)

    print('OPPORTUNITY {} from {} to {} Amount: {} {} Expected profit: {}%'.format(pair, exchange, opposite_exchange,
                                                                                   amount, pair.split('_')[0],
                                                                                   expected_profit))

    if expected_profit > 0:
        print('Arbitrage')
        threads = []
        result1 = {}
        th1 = threading.Thread(target=execute,
                               kwargs={"exchange": exchange,
                                       "symbol": pair,
                                       "side": "buy",
                                       "amount": amount,
                                       "price": exchange_price,
                                       "response": result1})
        th1.start()
        threads.append(th1)

        result2 = {}
        th2 = threading.Thread(target=execute,
                               kwargs={"exchange": opposite_exchange,
                                       "symbol": pair,
                                       "side": "sell",
                                       "amount": amount,
                                       "price": opposite_exchange_price,
                                       "response": result2})
        th2.start()
        threads.append(th2)

        threads[0].join()
        threads[1].join()

        print('Arbitrage results', result1, result2)


def launch():
    # settings
    exchanges = ['Bibox', 'Kucoin']

    # look for opportunities
    while True:
        excluded_exchanges = []
        for exchange in exchanges:
            for opposite_exchange in exchanges:
                if opposite_exchange != exchange and opposite_exchange not in excluded_exchanges:
                    find_opportunity(exchange, opposite_exchange)
                    excluded_exchanges.append(exchange)


launch()
