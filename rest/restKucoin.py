import traceback

import ccxt
from ccxt.base.errors import *

from utils import get_preferences

name = 'Kucoin'
kucoin = ccxt.kucoin2({
    'apiKey': get_preferences(name)['api_key'],
    'secret': get_preferences(name)['secret_key'],
    'password': get_preferences(name)['passphrase']
})


def get_balance(symbol=''):
    try:
        balances = kucoin.fetch_balance()
        if balances['info']:
            if symbol == '':
                result = []
                for balance in balances['info']['assets_list']:
                    result.append({'token': balance['coin_symbol'], 'quantity': balance['balance']})
                return result
            else:
                dct = dict((x['coin_symbol'], x['balance']) for x in balances['info']['assets_list'])
                return 0 if symbol not in dct else dct[symbol]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


def place_order(symbol, side, amount, price):
    try:
        symbol.replace('_', '/')
        # print((symbol, amount, price))
        if side == 'buy':
            order = kucoin.create_limit_buy_order(symbol, amount, price)
            return order['id']
        if side == 'sell':
            order = kucoin.create_limit_sell_order(symbol, amount, price)
            return order['id']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


def get_order_status(id, symbol):
    try:
        symbol.replace('_', '/')
        open_orders = kucoin.fetch_open_orders(symbol)
        if open_orders is not None:
            for order in open_orders:
                if int(order['id']) == id:
                    return order['status']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


def cancel_order(id, symbol):
    try:
        symbol.replace('_', '/')
        return kucoin.cancel_order(id, symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


def get_trade_limits(symbol=''):
    try:
        ms = kucoin.load_markets()
        if symbol != '':
            symbol.replace('_', '/')
        result = []
        if ms is not None:
            for m in ms:
                minimum = float(ms[m]['limits']['amount']['min'])
                if symbol != '' and symbol == m.replace('/', '_'):
                    return {'symbol': symbol.replace('/', '_'), 'min': minimum, 'market': minimum}
                result.append({'symbol': m.replace('/', '_'), 'min': minimum, 'market': minimum})
        return result
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())

# ------------ usage ------------

# print(get_balance())
# print(get_balance('BTC'))
# print(get_trade_limits())
# print(get_trade_limits('ETH_USDT'))
