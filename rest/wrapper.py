import traceback
from rest import restKucoin as kucoin, restBibox as bibox


def get_exchange(exchange):
    if exchange == 'Kucoin':
        return kucoin
    if exchange == 'Bibox':
        return bibox


def place_limit_order(exchange, symbol, side, amount, price):
    try:
        return get_exchange(exchange).place_order(symbol, side, amount, price)
    except:
        print(traceback.format_exc())
